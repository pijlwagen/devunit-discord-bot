### Werken aan de Discord bot
Je kunt zelf een instantie van deze bot draaien op je computer. Om dit te doen moet je wel eerst een bot aanmaken op het Discord [developer portaal](https://discord.com/developers/applications). Maak daar een applicatie en binnen deze applicatie kun je een bot maken.

De bot is geschreven in TypeScript, TypeScript is JavaScript maar dan met een mooi handschrift. Je kunt dus gewoon normaal JavaScript schrijven.

#### Omgeving opzetten
Er zijn een aantal nodig om de bot werkende te krijgen, als eerste is dat [Node.js](https://nodejs.org/en). Node.js zorgt er voor dat je JavaScript op je computer kunt draaien, in plaats van alleen in de browser. Druk op de knop met de 'LTS' en loop de installer door.

Als je Node.js hebt geinstalleerd word de volgende stap op een `.env` bestand te maken. Kopieer dit bestand van `.env.example`. Het `.env` bestand is ons configuratiebestand waarin de token en dergelijke komen te staan. Als je het bestand hebt gemaakt kun je de token van de bot in dit bestand zetten, samen met de applicatie ID.

Het laatste stapje is dan om een terminal window te openen in de map van de bot, typ vervolgens in deze terminal `npm i` om alle benodigde packages te installeren. Als deze klaar is kun je de bot starten met `npm run dev`.

## Waar staat wat
### `src/`
Hier staat alle code van de bot. In `index.ts` vind je de instantie die inlogt op Discord en in `deploy-commands.ts` vind je het script dat de slash commands upload naar Discord.

### `src/commands`
Hier staan alle commandos in van de bot, momenteel zijn dat alleen slash commands maar er is een mogelijkheid om ook gewoon chat commands te doen.

### `src/events`
Hier staan alle events, zoals het ontvangen van een bericht of wanneer er een interacitie (slash command, modal) word uitgevoerd.

### `src/utilities`
Helper functies en dergelijke.