-- CreateTable
CREATE TABLE "Checkin" (
    "id" TEXT NOT NULL,
    "date" TEXT NOT NULL,
    "startTime" TEXT NOT NULL,
    "endTime" TEXT,
    "goal" TEXT NOT NULL,
    "goalAchieved" INTEGER,
    "reflection" TEXT,

    PRIMARY KEY ("id", "date")
);
