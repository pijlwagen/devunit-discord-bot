import { ActionRowBuilder, Channel, EmbedBuilder, GuildMember, GuildTextBasedChannel, ModalActionRowComponentBuilder, ModalBuilder, ModalSubmitInteraction, TextInputBuilder, TextInputStyle } from "discord.js";
import { Colors } from "../utilities/colors";
import { format } from "date-fns";
import config from "../utilities/config";
import Logger from "../utilities/logger";
import { PrismaClient } from "@prisma/client";

class CheckinModal {
    create(): ModalBuilder {
        const modal = new ModalBuilder()
            .setCustomId('du-checkin-modal')
            .setTitle('Dagelijkse check-in');

        const goalInput = new TextInputBuilder()
            .setCustomId('du-checkin-modal-goal')
            .setLabel('Wat is je doel voor vandaag?')
            .setMinLength(32)
            .setMaxLength(1024)
            .setRequired(true)
            .setStyle(TextInputStyle.Paragraph)

        const actionRow: ActionRowBuilder<ModalActionRowComponentBuilder> = new ActionRowBuilder<ModalActionRowComponentBuilder>()
            .addComponents(goalInput);

        modal.addComponents(actionRow);

        return modal;
    }

    async handle(interaction: ModalSubmitInteraction, prisma: PrismaClient) {
        const client = interaction.client;
        const goal = interaction.fields.getTextInputValue('du-checkin-modal-goal')

        let member = await interaction.guild?.members.cache.find((m: GuildMember) => m.id === interaction.user.id);
        if (!member) member = await interaction.guild?.members.fetch(interaction.user.id);
        if (!member) return;

        const startTime = format(new Date(), 'HH:mm');
        const date = format(new Date(), 'yyyy-MM-dd');

        await prisma.checkin.create({
            data: {
                startTime: startTime,
                id: interaction.user.id,
                date: date,
                goal: goal
            }
        });

        const embed = new EmbedBuilder()
            .setAuthor({
                name: 'Check-in',
                iconURL: client.user.displayAvatarURL()
            })
            .setTitle(`${member.displayName} is ingecheckt.`)
            .addFields([
                {
                    name: 'Tijd',
                    value: startTime
                },
                {
                    name: 'Dagdoel',
                    value: goal as string
                }
            ])
            .setTimestamp()
            .setColor(Colors.GREEN);

        const channel: GuildTextBasedChannel = (client.channels.cache.find((c: Channel) => c.id === config.CHECKIN_CHANNEL) || await client.channels.fetch(config.CHECKIN_CHANNEL)) as GuildTextBasedChannel;
        if (!channel) return Logger.warn('[src/modals/checkin-modal.ts] Unable to find checkin-checkout channel.');

        await channel.send({ embeds: [embed.toJSON()] });
        await interaction.reply({
            embeds: [
                new EmbedBuilder().setColor(Colors.GREEN).setTitle('Check-in succesvol.').setDescription('Je bent ingecheckt, vergeet niet om uit te checken aan het einde van de dag. Succes!').toJSON()
            ],
            ephemeral: true
        });
    }
}

export default CheckinModal;