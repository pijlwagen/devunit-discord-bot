import { ActionRowBuilder, Channel, EmbedBuilder, GuildMember, GuildTextBasedChannel, ModalActionRowComponentBuilder, ModalBuilder, ModalSubmitInteraction, StringSelectMenuBuilder, StringSelectMenuOptionBuilder, TextInputBuilder, TextInputStyle } from "discord.js";
import { Colors } from "../utilities/colors";
import { differenceInMilliseconds, format, intervalToDuration, parse } from "date-fns";
import Logger from "../utilities/logger";
import config from "../utilities/config";
import { PrismaClient } from "@prisma/client";

class CheckoutModal {
    create(): ModalBuilder {
        const modal = new ModalBuilder()
            .setCustomId('du-checkout-modal')
            .setTitle('Dagelijkse check-out');

        const goalAchieved = new TextInputBuilder()
            .setCustomId('du-checkout-modal-goal-achieved')
            .setLabel('Heb je je doel bereikt (ja/nee).')
            .setMaxLength(3)
            .setMinLength(2)
            .setRequired(true)
            .setStyle(TextInputStyle.Short);

        const goalInput = new TextInputBuilder()
            .setCustomId('du-checkout-modal-goal')
            .setLabel('Hoe ging het vandaag?')
            .setMinLength(32)
            .setMaxLength(1024)
            .setRequired(true)
            .setStyle(TextInputStyle.Paragraph)

        const actionRowOne: ActionRowBuilder<ModalActionRowComponentBuilder> = new ActionRowBuilder<ModalActionRowComponentBuilder>()
            .addComponents(goalAchieved);
            
        const actionRowTwo: ActionRowBuilder<ModalActionRowComponentBuilder> = new ActionRowBuilder<ModalActionRowComponentBuilder>()
            .addComponents(goalInput);

        modal.addComponents(actionRowOne, actionRowTwo);

        return modal;
    }

    async handle(interaction: ModalSubmitInteraction, prisma: PrismaClient) {
        const client = interaction.client;
        const reflection = interaction.fields.getTextInputValue('du-checkout-modal-goal');
        const goalAchieved = interaction.fields.getTextInputValue('du-checkout-modal-goal-achieved');


        let member = await interaction.guild?.members.cache.find((m: GuildMember) => m.id === interaction.user.id);
        if (!member) member = await interaction.guild?.members.fetch(interaction.user.id);
        if (!member) return;

        const endTime = format(new Date(), 'HH:mm');
        const date = format(new Date(), 'yyyy-MM-dd');

        const checkin = await prisma.checkin.update({
            where: {
                id_date: {
                    id: interaction.user.id,
                    date: date 
                }
            },
            data: {
                endTime: endTime,
                goalAchieved: Number(goalAchieved.toLowerCase() == 'ja'),
                reflection: reflection
            }
        });

        const startTimeDate = parse(`${checkin.date} ${checkin.startTime}`, 'yyyy-MM-dd HH:mm', new Date());
        const endTimeDate = parse(`${checkin.date} ${checkin.endTime}`, 'yyyy-MM-dd HH:mm', new Date());
        const timeDifferenceInMilliseconds = differenceInMilliseconds(startTimeDate, endTimeDate);
        const duration = format(new Date(timeDifferenceInMilliseconds), 'HH:mm');

        const embed = new EmbedBuilder()
            .setAuthor({
                name: 'Check-out',
                iconURL: client.user.displayAvatarURL()
            })
            .setTitle(`${member.displayName} is uitgecheckt.`)
            .addFields([
                {
                    name: 'Tijd',
                    value: `**In:** ${format(startTimeDate, 'HH:mm')}\n**Uit:** ${endTime})`
                },
                {
                    name: 'Dagdoel bereikt?',
                    value: goalAchieved.toLowerCase() == 'ja'? 'Ja' : 'Nee'
                },
                {
                    name: 'Hoe ging het?',
                    value: reflection
                }
            ])
            .setTimestamp()
            .setColor(Colors.RED);

            const channel: GuildTextBasedChannel = (client.channels.cache.find((c: Channel) => c.id === config.CHECKIN_CHANNEL) || await client.channels.fetch(config.CHECKIN_CHANNEL)) as GuildTextBasedChannel;
            if (!channel) return Logger.warn('[src/modals/checkout-modal.ts] Unable to find checkin-checkout channel.');

        await channel.send({ embeds: [embed.toJSON()] });
        await interaction.reply({
            embeds: [
                new EmbedBuilder().setColor(Colors.GREEN).setTitle('Check-out succesvol.').setDescription('Je bent uitgecheckt, tot de volgende keer!').toJSON()
            ],
            ephemeral: true
        });
    }
}

export default CheckoutModal;