import {  CacheType, CommandInteraction, EmbedBuilder, SlashCommandBuilder } from "discord.js";
import { IBaseSlashCommand } from "../utilities/command";
import Logger from "../utilities/logger";
import CheckinModal from "../modals/checkin-modal";
import { PrismaClient } from "@prisma/client";
import { format } from "date-fns";
import { Colors } from "../utilities/colors";

// @ts-expect-error
export default class CheckinCommand implements IBaseSlashCommand {
    static commandName = 'checkin';
    name = CheckinCommand.commandName;
    description = 'Tijdens een thuiswerkdag kun je via dit command inchecken.';
    enabled = true;
    enabledInDM = false;
    interaction: CommandInteraction;

    constructor(interaction?: CommandInteraction<CacheType> | null | undefined) {
        this.interaction = interaction as CommandInteraction;
        return this;
    }

    build() {
        return new SlashCommandBuilder()
        .setDMPermission(this.enabledInDM)
        .setName(this.name)
        .setDescription(this.description);
    }
    
    async execute(prisma: PrismaClient) {
        try {
            const exists = await this.doesCheckinExist(prisma);

            if (exists) {
                await this.interaction.reply({
                    embeds: [
                        new EmbedBuilder().setColor(Colors.ORANGE).setTitle('Je bent al ingecheckt.').setDescription('Je bent al ingecheckt vandaag. Je kunt uitchecken via `/checkout`').toJSON()
                    ],
                    ephemeral: true
                })
                return;
            }

            const modal = (new CheckinModal()).create();
            await this.interaction.showModal(modal);
        } catch (e: any) {
            Logger.error(`checkin.ts: ${e.message}`)
        } 
    }

    async doesCheckinExist(prisma: PrismaClient): Promise<boolean> {
        const now = new Date();
        const date = format(now, 'yyyy-MM-dd');

        const checkin = await prisma.checkin.findFirst({
            where: {
                id: this.interaction.user.id,
                date: date
            }
        });

        return !!checkin;
    }
}