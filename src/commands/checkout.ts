import {  CacheType, CommandInteraction, EmbedBuilder, SlashCommandBuilder } from "discord.js";
import { IBaseSlashCommand } from "../utilities/command";
import Logger from "../utilities/logger";
import CheckoutModal from "../modals/checkout-modal";
import { Checkin, PrismaClient } from "@prisma/client";
import { format } from "date-fns";
import { Colors } from "../utilities/colors";

// @ts-expect-error
export default class CheckoutCommand implements IBaseSlashCommand {
    static commandName = 'checkout';
    name = CheckoutCommand.commandName;
    description = 'Tijdens een thuiswerkdag kun je via dit command uitchecken.';
    enabled = true;
    enabledInDM = false;
    interaction: CommandInteraction;

    constructor(interaction?: CommandInteraction<CacheType> | null | undefined) {
        this.interaction = interaction as CommandInteraction;
        return this;
    }

    build() {
        return new SlashCommandBuilder()
        .setDMPermission(this.enabledInDM)
        .setName(this.name)
        .setDescription(this.description);
    }
    
    async execute(prisma: PrismaClient) {
        try {
            const checkin = await this.getExistingCheckin(prisma);

            if (!checkin) {
                await this.interaction.reply({
                    embeds: [
                        new EmbedBuilder().setColor(Colors.RED).setTitle('Je bent niet ingecheckt.').setDescription('Je bent vandaag niet ingecheckt, en kunt daarom dus ook niet uitchecken.').toJSON()
                    ],
                    ephemeral: true
                })
                return;
            } else if (checkin.endTime) {
                await this.interaction.reply({
                    embeds: [
                        new EmbedBuilder().setColor(Colors.RED).setTitle('Je bent al uitgecheckt.').setDescription('Je bent al uitgecheckt, je kunt morgen pas weer inchecken.').toJSON()
                    ],
                    ephemeral: true
                })
                return;
            }

            const modal = (new CheckoutModal()).create();
            await this.interaction.showModal(modal);
        } catch (e: any) {
            Logger.error(`checkout.ts: ${e.message}`);
        } 
    }

    async getExistingCheckin(prisma: PrismaClient): Promise<Partial<Checkin>> {
        const now = new Date();
        const date = format(now, 'yyyy-MM-dd');

        const checkin = await prisma.checkin.findFirst({
            where: {
                id: this.interaction.user.id,
                date: date
            }
        });

        return checkin as Partial<Checkin>;
    }
}