import { CacheType, ChatInputCommandInteraction, CommandInteraction, EmbedBuilder, GuildMember, GuildTextBasedChannel, Role, SlashCommandBuilder, SlashCommandStringOption } from "discord.js";
import { IBaseSlashCommand } from "../utilities/command";
import Logger from "../utilities/logger";
import { Colors } from "../utilities/colors";
import config from "../utilities/config";

// @ts-expect-error
export default class CheckinCommand implements IBaseSlashCommand {
    static commandName = 'aanmelden';
    name = CheckinCommand.commandName;
    description = 'Nieuw in de Discord? Meld je aan via dit commando om toegang te krijgen tot de kanalen.';
    enabled = true;
    enabledInDM = false;
    interaction: ChatInputCommandInteraction;

    constructor(interaction?: CommandInteraction<CacheType> | null | undefined) {
        this.interaction = interaction as ChatInputCommandInteraction;
        return this;
    }

    build() {
        return new SlashCommandBuilder()
            .setDMPermission(this.enabledInDM)
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption((input: SlashCommandStringOption) => {
                return input.setMinLength(6)
                    .setMaxLength(6)
                    .setName('studentnummer')
                    .setDescription('Jouw studentnummer.')
                    .setRequired(true);
            })
            .addStringOption((input: SlashCommandStringOption) => {
                return input
                    .setName('naam')
                    .setDescription('Jouw volledige naam.')
                    .setRequired(true);
            })
    }

    async execute() {
        try {
            const alreadyVerfied = await this.isAlreadyVerified();

            if (alreadyVerfied) {
                await this.interaction.reply({
                    content: 'Je bent al aangemeld.',
                    ephemeral: true
                });
                return;
            }
            
            const name: string = this.interaction.options.getString('naam', true);
            const id: string = this.interaction.options.getString('studentnummer', true);
            const user = this.interaction.user;
            const channel: GuildTextBasedChannel | undefined = (this.interaction.client.channels.cache
                .find(c => c.id === config.SIGNUP_CHANNEL_ID) || await this.interaction.client.channels.fetch(config.SIGNUP_CHANNEL_ID)) as GuildTextBasedChannel | undefined;

            if (!channel) return Logger.error('[src/commands/sign-up.ts] No sign-up channel specified, or no access to it.')

            const embed = new EmbedBuilder()
            .setColor(Colors.GREEN)
            .setTitle('Nieuwe Discord aanmelding')
            .setThumbnail(this.interaction.client.user.displayAvatarURL())
            .addFields([
                {
                    name: 'Volledige naam',
                    value: name,
                },
                {
                    name: 'Studentnummer',
                    value: id,
                },
            ]);


            await channel.send({
                content: `<@${user.id}>`,
                embeds: [embed]
            });

            await this.interaction.reply({
                content: 'Je aanmelding is ontvangen, het kan even duren voordat je geverifieerd bent.',
                ephemeral: true
            })
        } catch (e: any) {
            Logger.error(`sign-up.ts: ${e.message}`)
        }
    }

    async isAlreadyVerified(): Promise<boolean> {
        let member = this.interaction.member;
        if (!member) await this.interaction.guild?.members.fetch(this.interaction.user.id);

        if (!member) return true;

        return !!(member as GuildMember).roles.cache.find((role: Role) => role.name === 'Student');
    }
}