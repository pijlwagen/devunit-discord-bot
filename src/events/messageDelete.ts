import { AuditLogEvent, EmbedBuilder, GuildAuditLogs, GuildAuditLogsEntry, GuildBasedChannel, GuildMember, GuildTextBasedChannel, Message, Role } from "discord.js";
import { Colors } from "../utilities/colors";
import { BaseEventHandler } from "../utilities/baseEventHandler";
import config from "../utilities/config";
import Logger from "../utilities/logger";

export default class OnMessageDelete implements BaseEventHandler {
    static eventName = 'messageDelete';
    eventName = OnMessageDelete.eventName;
    message: Message;

    constructor(message: Message) {
        this.message = message;
        try {
            this.handle();
        } catch (e) {
            console.error(e);
        }
    }

    async handle() {
        if (this.message.author.bot) return;
        if (!this.message.guild) return;
        if (!this.message.member) return;
        if (this.message.partial) return;
        if (!this.message.content) return;

        let member: GuildMember = this.message.member;
        if (!member) await this.message.member.fetch();

        if (member.roles.cache.find((role: Role) => {
            return ['Moderator', 'Coach'].includes(role.name);
        })) return;

        const channel = await this.getLogChannel();
        if (!channel) return Logger.warn('[src/events/messageDelete.ts] Unable to find logging channel.');
        const embed: EmbedBuilder = this.buildEmbed(member);

        const logs: GuildAuditLogs = await this.message.guild.fetchAuditLogs({
            limit: 10,
            type: AuditLogEvent.MessageDelete
        });

        if (logs.entries.size > 0) {
            const entry = logs.entries.find((entry: GuildAuditLogsEntry) => entry.targetId === this.message.author.id);
            if (entry) {
                embed.addFields([{
                    name: 'Verwijderd door', value: `<@${entry.executorId}>`, inline: true
                }])
            }
        }

        channel.send({ embeds: [embed] });
    }

    private buildEmbed(member: GuildMember) {
        const builder = new EmbedBuilder();
        builder.setColor(Colors.RED);
        builder.setAuthor({
            name: 'Bericht verwijderd'
        });
        builder.setThumbnail(this.message.client.user.displayAvatarURL());
        builder.setTitle('Inhoud');
        builder.setDescription(this.message.cleanContent.substring(0, 2048));
        builder.addFields([
            { name: 'Verstuurd door', value: member.displayName, inline: true },
            { name: 'Kanaal', value: `<#${this.message.channelId}>`, inline: true }
        ]);
        builder.setTimestamp();

        return builder;
    }

    private async getLogChannel(): Promise<GuildTextBasedChannel | undefined> {
        let channel = this.message.guild?.channels.cache.find((c: GuildBasedChannel) => c.id === config.LOGGING_CHANNEL);

        if (!channel) {
            channel = (await this.message.guild?.channels.fetch(config.LOGGING_CHANNEL)) || undefined;
        }

        if (!channel) return undefined;

        return channel as GuildTextBasedChannel;
    }
}