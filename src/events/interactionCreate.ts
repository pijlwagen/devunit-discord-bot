import { CommandInteraction, Interaction, Message, ModalSubmitInteraction } from "discord.js";
import { BaseEventHandler } from "../utilities/baseEventHandler";
import { CommandHandler, IBaseSlashCommand } from "../utilities/command";
import CheckinModal from "../modals/checkin-modal";
import CheckoutModal from "../modals/checkout-modal";
import { PrismaClient } from "@prisma/client";

export default class OnInteractionCreate implements BaseEventHandler {
    static eventName = 'interactionCreate';
    eventName = OnInteractionCreate.eventName;
    interaction: Interaction;
    commandHandler: CommandHandler;
    prisma: PrismaClient;

    constructor(
        interaction: Interaction,
        commandHandler: CommandHandler,
        prisma: PrismaClient
    ) {
        this.interaction = interaction;
        this.commandHandler = commandHandler;
        this.prisma = prisma;

        try {
            if (interaction.isCommand()) this.executeSlashCommand();
            if (interaction.isModalSubmit()) this.executeModalSubmit();
        } catch (e) {
            console.error(e);
        }
    }

    executeSlashCommand() {
        const interaction: CommandInteraction = this.interaction as CommandInteraction;
        const command: IBaseSlashCommand | undefined = this.commandHandler
            .getSlashCommands()
            .find((cmd: IBaseSlashCommand) => cmd.commandName === interaction.commandName);

        if (command) {
            const instance = new command(interaction);
            instance.execute(this.prisma);
        }
    }

    executeModalSubmit() {
        const interaction: ModalSubmitInteraction = this.interaction as ModalSubmitInteraction;
        if (interaction.customId === 'du-checkin-modal') {
            (new CheckinModal()).handle(interaction, this.prisma);
        } else if (interaction.customId === 'du-checkout-modal') {
            (new CheckoutModal()).handle(interaction, this.prisma);
        }

    }
}