import { AuditLogEvent, EmbedBuilder, GuildAuditLogs, GuildAuditLogsEntry, GuildBasedChannel, GuildMember, GuildTextBasedChannel, Message, Role } from "discord.js";
import { Colors } from "../utilities/colors";
import { BaseEventHandler } from "../utilities/baseEventHandler";
import config from "../utilities/config";

export default class OnMessageUpdate implements BaseEventHandler {
    static eventName = 'messageUpdate';
    eventName = OnMessageUpdate.eventName;
    oldMessage: Message;
    message: Message

    constructor(oldMessage: Message, newMessage: Message) {
        this.oldMessage = oldMessage;
        this.message = newMessage;

        try {
            this.handle();
        } catch (e) {
            console.error(e);
        }
    }

    async handle() {
        if (this.message.author.bot) return;
        if (!this.message.guild) return;
        if (!this.message.member) return;
        if (this.message.partial) return;
        if (this.message.content === this.oldMessage.content) return;

        const member: GuildMember = await this.message.member.fetch();

        if (member.roles.cache.find((role: Role) => {
            return ['Moderator', 'Coach'].includes(role.name);
        })) return;

        const channel = await this.getLogChannel();
        if (!channel) return;
        const embed: EmbedBuilder = this.buildEmbed(member);

        channel.send({ embeds: [embed] });
    }

    private buildEmbed(member: GuildMember) {
        const builder = new EmbedBuilder();
        builder.setColor(Colors.ORANGE);
        builder.setAuthor({
            name: 'Bericht gewijzigd'
        });
        builder.setThumbnail(this.message.client.user.displayAvatarURL());
        builder.addFields([
            { name: 'Oude inhoud', value: this.oldMessage.cleanContent.substring(0, 1024) },
            { name: 'Nieuwe inhoud', value: this.message.cleanContent.substring(0, 1024) },
            { name: 'Verstuurd door', value: member.displayName, inline: true },
            { name: 'Kanaal', value: `<#${this.message.channelId}>`, inline: true }
        ]);
        builder.setTimestamp();

        return builder;
    }

    private async getLogChannel(): Promise<GuildTextBasedChannel | undefined> {
        let channel = this.message.guild?.channels.cache.find((c: GuildBasedChannel) => c.id === config.LOGGING_CHANNEL);

        if (!channel) {
            channel = (await this.message.guild?.channels.fetch(config.LOGGING_CHANNEL)) || undefined;
        }

        if (!channel) return undefined;

        return channel as GuildTextBasedChannel;
    }
}