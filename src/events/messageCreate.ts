import { Message } from "discord.js";
import { BaseEventHandler } from "../utilities/baseEventHandler";
import Logger from "../utilities/logger";

export default class OnMessageCreate implements BaseEventHandler {
    static eventName = 'messageCreate';
    eventName = OnMessageCreate.eventName;
    message: Message;

    constructor(message: Message) {
        this.message = message;
        // this.handle();
    }
}