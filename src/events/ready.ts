import { ActionRowBuilder, Client, ComponentType, GuildMember, Message, Role, RoleSelectMenuBuilder, StringSelectMenuBuilder, StringSelectMenuComponent, StringSelectMenuInteraction, StringSelectMenuOptionBuilder, TextChannel } from "discord.js";
import Logger from "../utilities/logger";
import { BaseEventHandler } from "../utilities/baseEventHandler";

export default class OnReady implements BaseEventHandler {
    static eventName = 'ready';
    eventName = OnReady.eventName;
    client: Client;

    constructor(client: Client) {
        this.client = client;
        Logger.info('[src/events/ready.ts] Logged into the API as ' + client.user?.tag);
        try {
            this.roleAssignment();
        } catch (e) {
            console.error(e);
        }
    }

    async roleAssignment() {
        const assignableRoles = [
            { value: 'Front-end', label: 'Front-end', description: 'Specialisatie in front-end.' },
            { value: 'Back-end', label: 'Back-end', description: 'Specialisatie in back-end.' },
            { value: 'Full-stack', label: 'Full-stack', description: 'Best of both worlds.' },
            { value: 'PHP', label: 'PHP', description: 'PHP: Hypertext Preprocessor.' },
            { value: 'XAMPP', label: 'XAMPP', description: 'Cross-Platform Apache MySQL PHP Perl.' },
            { value: 'MySQL', label: 'MySQL', description: 'My Structured Query Language.' },
            { value: 'HTML', label: 'HTML', description: 'Hypertext Markup Language.' },
            { value: 'CSS', label: 'CSS', description: 'Cascading Style Sheets.' },
            { value: 'Python', label: 'Python', description: 'Ssst, ik ben een slang' },
        ];

        const channel: TextChannel = await this.client.channels.fetch('1197985033940631595') as TextChannel;
        if (!channel) return;

        const existingMessages = await channel.messages.fetch({ limit: 20 });
        if (existingMessages.size > 0) await channel.bulkDelete(existingMessages.map((m: Message) => m.id));

        const select: StringSelectMenuBuilder = new StringSelectMenuBuilder()
            .setCustomId('select-programming-roles')
            .setPlaceholder('Selecteer rollen waar jij kennis van hebt.')
            .setMinValues(0)
            .setMaxValues(assignableRoles.length)
            .addOptions(
                assignableRoles.map(
                    (option) => new StringSelectMenuOptionBuilder()
                        .setDescription(option.description)
                        .setLabel(option.label)
                        .setValue(option.value)
                )
            )

        // TODO: investigate
        // @ts-ignore
        const row: ActionRowBuilder<StringSelectMenuBuilder> = new ActionRowBuilder().addComponents(select);

        const message = await channel.send({
            content: 'Selecteer hier technische onderwerpen waar jij verstand van hebt.',
            components: [row]
        });

        const collector = message.createMessageComponentCollector({
            componentType: ComponentType.StringSelect
        });

        collector.on('collect', async (interaction: StringSelectMenuInteraction) => {
            const roles = channel.guild.roles.cache.filter((role: Role) => assignableRoles.map(r => r.value).includes(role.name));
            const member: GuildMember = await channel.guild.members.fetch(interaction.user.id);

            if (!member.manageable) {
                await interaction.reply({ ephemeral: true, content: 'Ik kan je rollen niet beheren.' });
                return;
            }

            const selectedRoleNames: string[] = interaction.values;

            const rolesToAssign = roles.filter((role: Role) => !member.roles.cache.has(role.id) && selectedRoleNames.includes(role.name));
            const rolesToRemove = roles.filter((role: Role) => member.roles.cache.has(role.id) && !selectedRoleNames.includes(role.name));

            if (rolesToAssign.size > 0) {
                await member.roles.add(rolesToAssign);
            }

            if (rolesToRemove.size > 0) {
                await member.roles.remove(rolesToRemove);
            }

            const reply = await interaction.reply({
                ephemeral: true,
                content: `<@${member.id}> je hebt nu de rollen: ${interaction.values.join(', ') || 'geen'}.`
            });
            setTimeout(async () => {
                await reply.delete();
            }, 10_000);
        })
    }
}