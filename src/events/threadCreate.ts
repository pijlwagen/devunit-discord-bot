import { ForumChannel, GuildForumTag, Role, ThreadChannel } from "discord.js";
import { BaseEventHandler } from "../utilities/baseEventHandler";

export default class OnThreadCreate implements BaseEventHandler {
    static eventName = 'threadCreate';
    eventName = OnThreadCreate.eventName;
    thread: ThreadChannel;
    forum?: ForumChannel;

    constructor(thread: ThreadChannel) {
        this.thread = thread;
        if (!(thread.parent instanceof ForumChannel)) return;
        this.forum = thread.parent as ForumChannel;
        this.handle();
    }

    async handle() {
        if (!this.forum) return;
        if (this.forum.name !== 'Programmeer-vragen') return;
        const tags: GuildForumTag[] = this.forum.availableTags.filter((tag: GuildForumTag) => {
            return this.thread.appliedTags.includes(tag.id)
        });

        const roles = this.thread.guild.roles.cache.filter((role: Role) => tags.map(tag => tag.name).includes(role.name));

        if (roles.size === 0) return;

        this.thread.send('Gerelateerde rollen op basis van tags: ' + roles.map((role: Role) => `<@&${role.id}>`).join(' '));
    }
}