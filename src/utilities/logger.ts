import { add, format } from 'date-fns';
import { EmbedBuilder } from 'discord.js';
import { Colors } from './colors';

export default class Logger {
    static log(message: String, addEnding = true) {
        message = `[${format(new Date(), 'yyyy/LL/dd HH:mm:ss.SSS')}] ${message}`;

        if (addEnding && !message.endsWith('.')) {
            message += '.';
        }

        console.log(message);
    }

    static info(message: String) {
        Logger.log('[\x1b[100mINFO\x1b[0m] ' + message);
    }

    static error(message: String) {
        Logger.log('[\x1b[41mERROR\x1b[0m] ' + message);
    }

    static warn(message: String) {
        Logger.log('\x1b[43mWARN\x1b[0m] ' + message);  
    }

    static errorAsEmbed(message: string, error?: string): EmbedBuilder {
        const embed = new EmbedBuilder()
        .setColor(Colors.RED)
        .setTitle('Er is iets fout gegaan...')
        .setDescription(message + ', probeer het later opnieuw.');

        if (error) {
            embed.addFields([{ name: 'Foutmelding', value: error }]);
        }

        return embed;
    }
}