import { ApplicationCommandOption, CacheType, CommandInteraction, CommandInteractionResolvedData, SlashCommandBuilder } from "discord.js";
import path from 'path';
import fs from 'fs';
import Logger from "./logger";
import { PrismaClient } from "@prisma/client";
interface IBaseCommand {
    commandName: string;
    description: string;
    name: string;
    enabled: boolean;
    enabledInDM: boolean;

    new(...args: any[]): this;
}

interface IBaseSlashCommand extends IBaseCommand {
    options?: ApplicationCommandOption[];
    interaction: CommandInteraction | null;

    new(interaction?: CommandInteraction<CacheType> | null | undefined): this;
    execute(prisma?: PrismaClient): void | Promise<void>;
    build(): Omit<SlashCommandBuilder, "addSubcommand" | "addSubcommandGroup">
}

class CommandHandler {
    commands: IBaseCommand[] = [];
    directory = path.join(__dirname, '../commands');

    constructor() {
        const files: string[] = fs.readdirSync(this.directory);
        Logger.info(`[src/command.ts] Attempting to load ${files.length} commands.`);
        for (const file of files.filter((file: string) => file.endsWith('.ts') || file.endsWith('.js'))) {
            const commandLocation: string = path.join(this.directory, file);
            const commandFile = require(commandLocation).default;
            this.commands.push(commandFile);
        }
        Logger.info(`[src/command.ts] ${this.commands.length} were loaded.`);
    }

    getCommands(): IBaseCommand[] {
        return this.commands;
    }

    getSlashCommands(): IBaseSlashCommand[] {
        return this.commands.filter((command: IBaseCommand) => {
            return 'build' in new command();
        }) as IBaseSlashCommand[]
    }
}

export { IBaseCommand, IBaseSlashCommand, CommandHandler }