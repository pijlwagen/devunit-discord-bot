import * as dotenv from 'dotenv';

dotenv.config();

interface EnvConfig {
  DISCORD_TOKEN: string;
  DISCORD_CLIENT_ID: string;
  CHECKIN_CHANNEL: string;
  LOGGING_CHANNEL: string;
  SIGNUP_CHANNEL_ID: string
}

const config: EnvConfig = {
  DISCORD_TOKEN: process.env.DISCORD_TOKEN || '',
  DISCORD_CLIENT_ID: process.env.DISCORD_CLIENT_ID || '',
  CHECKIN_CHANNEL: process.env.CHECKIN_CHANNEL || '',
  LOGGING_CHANNEL: process.env.LOGGING_CHANNEL || '',
  SIGNUP_CHANNEL_ID: process.env.SIGNUP_CHANNEL_ID || ''
};

export default config;