import { REST, Routes } from 'discord.js';
import config from './utilities/config';
import { CommandHandler } from './utilities/command';
import Logger from './utilities/logger';

const commandHandler: CommandHandler = new CommandHandler();
const jsonCommands = [];

for (const command of commandHandler.getSlashCommands()) {
    const instance = new command();
    const builder = instance.build();
    jsonCommands.push(builder.toJSON());
}

const rest = new REST().setToken(config.DISCORD_TOKEN);

(async (): Promise<void> => {
    try {
        Logger.info('[src/deploy-commands.ts] Registering application commands.');

        const data: string[] = await rest.put(
            Routes.applicationCommands(config.DISCORD_CLIENT_ID),
            { body: jsonCommands }
        ) as string[];
        
        Logger.info(`[src/deploy-commands.ts] Registered ${data.length} commands.`)
    } catch (e: any) {
        Logger.error(e.message);
    }
})()
