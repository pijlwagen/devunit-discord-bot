import { Client, GatewayIntentBits } from 'discord.js';
import config from './utilities/config';
import Logger from './utilities/logger';
import OnMessageDelete from './events/messageDelete';
import OnReady from './events/ready';
import OnMessageCreate from './events/messageCreate';
import OnMessageUpdate from './events/messageUpdate';
import OnThreadCreate from './events/threadCreate';
import OnInteractionCreate from './events/interactionCreate';
import { CommandHandler } from './utilities/command';
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

const client = new Client({
    intents: [
        GatewayIntentBits.GuildMembers, 
        GatewayIntentBits.GuildMessages, 
        GatewayIntentBits.MessageContent, 
        GatewayIntentBits.Guilds
    ]
});

const eventHandlers = [
    OnMessageDelete, 
    OnReady, 
    OnMessageCreate, 
    OnMessageUpdate, 
    OnThreadCreate,
    OnInteractionCreate
];

const commandHandler = new CommandHandler();

for (const eventHandler of eventHandlers) {
    Logger.info('[src/index.ts] Registered event handler ' + eventHandler.eventName);
    client.on(eventHandler.eventName, (...args: any[]) => {
        // @ts-ignore
        new eventHandler(...args, commandHandler, prisma);
    });
}

Logger.info('[src/index.ts] Logging into the API.');
client.login(config.DISCORD_TOKEN).catch(e => { Logger.error(e.message); });